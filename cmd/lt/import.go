package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"github.com/spf13/cobra"
)

const ledgerBookingEntryTemplate = `{{formatDate .BookedAt}} {{ if .Confirmed }}* {{ end }}{{.Description}}
    {{.Account}}  {{.Value}} €
`

func generateImportCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use: "import",
		RunE: func(cmd *cobra.Command, args []string) error {
			return nil
		},
	}
	cmd.AddCommand(generateImportEasybankCSVCmd())
	cmd.AddCommand(generateImportSparkasseJSONCmd())
	cmd.AddCommand(generateImportN26CSVCmd())
	return cmd
}

type importRecord struct {
	Description string
	Currency    string
	BookedAt    time.Time
	Value       decimal.Decimal
	Confirmed   bool
	Account     string
}

type importRecordByDate []importRecord

func (a importRecordByDate) Len() int           { return len(a) }
func (a importRecordByDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a importRecordByDate) Less(i, j int) bool { return a[i].BookedAt.Before(a[j].BookedAt) }

//go:generate go run gitlab.com/zerok/csvgen/cmd/csvgen --output gen_csvprocessors.go
type EasybankCSVProcessor struct {
	reader *csv.Reader
}

func (p *EasybankCSVProcessor) parseDate(ctx context.Context, value string) (time.Time, error) {
	loc, err := time.LoadLocation("Europe/Vienna")
	if err != nil {
		return time.Now(), err
	}
	return time.ParseInLocation("02.01.2006", value, loc)
}

func (p *EasybankCSVProcessor) parseValue(ctx context.Context, s string) (decimal.Decimal, error) {
	normalized := strings.ReplaceAll(strings.ReplaceAll(s, ".", ""), ",", ".")
	return decimal.NewFromString(normalized)
}

type N26CSVProcessor struct {
	reader *csv.Reader
}

func (p *N26CSVProcessor) parseDate(ctx context.Context, value string) (time.Time, error) {
	loc, err := time.LoadLocation("Europe/Vienna")
	if err != nil {
		return time.Now(), err
	}
	return time.ParseInLocation("2006-01-02", value, loc)
}

func (p *N26CSVProcessor) parseValue(ctx context.Context, s string) (decimal.Decimal, error) {
	normalized := strings.ReplaceAll(strings.ReplaceAll(s, ".", ""), ",", ".")
	return decimal.NewFromString(normalized)
}

func findFile(ctx context.Context, pattern string) (io.ReadCloser, error) {
	matches, err := filepath.Glob("/Users/zerok/Downloads/" + pattern)
	if err != nil {
		return nil, err
	}
	sort.Sort(sort.Reverse(sort.StringSlice(matches)))
	if len(matches) == 0 {
		return nil, fmt.Errorf("no file found")
	}
	log.Printf("Found %s", matches[0])
	return os.Open(matches[0])
}

func generateImportEasybankCSVCmd() *cobra.Command {
	var account string
	var confirmed bool
	var doFindFile bool
	tmpl := template.Must(template.New("root").Funcs(map[string]interface{}{
		"formatDate": func(val time.Time) string {
			return val.Format("2006/01/02")
		},
	}).Parse(ledgerBookingEntryTemplate))
	cmd := &cobra.Command{
		Use: "easybank-csv",
		RunE: func(cmd *cobra.Command, args []string) error {
			var err error
			if account == "" {
				return fmt.Errorf("please specify an account")
			}
			var input io.ReadCloser
			if doFindFile {
				input, err = findFile(cmd.Context(), "EASYBANK_Umsatzliste*.csv")
				if err != nil {
					return fmt.Errorf("failed to find a file for importing")
				}
				defer input.Close()
			} else {
				input = os.Stdin
			}
			proc := NewEasybankCSVProcessor(input)
			bookings, err := proc.ReadAll(cmd.Context())
			if err != nil {
				return err
			}
			sort.Sort(importRecordByDate(bookings))
			for _, b := range bookings {
				b.Account = account
				b.Confirmed = confirmed
				if err := tmpl.Execute(os.Stdout, b); err != nil {
					return err
				}
			}
			return nil
		},
	}
	cmd.Flags().BoolVar(&doFindFile, "find-file", false, "Try to find the relevant file automatically")
	cmd.Flags().StringVar(&account, "account", "", "Account to target with this booking")
	cmd.Flags().BoolVar(&confirmed, "confirmed", false, "Are these bookings confirmed?")
	return cmd
}

type sparkasseBooking struct {
	Note    string `json:"note"`
	Booking string `json:"booking"`
	Amount  struct {
		Currency  string  `json:"currency"`
		Value     float64 `json:"value"`
		Precision int32   `json:"precision"`
	} `json:"amount"`
	PartnerName string `json:"partnerName"`
	Reference   string `json:"reference"`
}

func generateImportSparkasseJSONCmd() *cobra.Command {
	var account string
	var confirmed bool
	var doFindFile bool
	tmpl := template.Must(template.New("root").Funcs(map[string]interface{}{
		"formatDate": func(val time.Time) string {
			return val.Format("2006/01/02")
		},
	}).Parse(ledgerBookingEntryTemplate))
	cmd := &cobra.Command{
		Use: "sparkasse-json",
		RunE: func(cmd *cobra.Command, args []string) error {
			if account == "" {
				return fmt.Errorf("please specify an account")
			}
			var input io.ReadCloser
			var err error
			if doFindFile {
				input, err = findFile(cmd.Context(), "AT54*.json")
				if err != nil {
					return fmt.Errorf("failed to find a file for importing")
				}
				defer input.Close()
			} else {
				input = os.Stdin
			}
			data := make([]sparkasseBooking, 0, 10)
			if err := json.NewDecoder(input).Decode(&data); err != nil {
				return err
			}
			bookings := make([]importRecord, 0, len(data))
			for _, d := range data {
				description := d.Note
				if description == "" {
					description = d.PartnerName
				}
				if description == "" {
					description = d.Reference
				}
				t, err := time.Parse("2006-01-02T15:04:05.000-0700", d.Booking)
				if err != nil {
					return err
				}
				value := decimal.NewFromFloat(d.Amount.Value).Div(decimal.NewFromInt32(10).Pow(decimal.NewFromInt32(d.Amount.Precision)))
				bookings = append(bookings, importRecord{
					Description: description,
					Currency:    d.Amount.Currency,
					BookedAt:    t,
					Value:       value,
					Confirmed:   confirmed,
				})
			}
			sort.Sort(importRecordByDate(bookings))
			for _, b := range bookings {
				b.Account = account
				b.Confirmed = confirmed
				if err := tmpl.Execute(os.Stdout, b); err != nil {
					return err
				}
			}
			return nil
		},
	}
	cmd.Flags().BoolVar(&doFindFile, "find-file", false, "Try to find the relevant file automatically")
	cmd.Flags().StringVar(&account, "account", "", "Account to target with this booking")
	cmd.Flags().BoolVar(&confirmed, "confirmed", false, "Are these bookings confirmed?")
	return cmd
}

func generateImportN26CSVCmd() *cobra.Command {
	var account string
	var confirmed bool
	tmpl := template.Must(template.New("root").Funcs(map[string]interface{}{
		"formatDate": func(val time.Time) string {
			return val.Format("2006/01/02")
		},
	}).Parse(ledgerBookingEntryTemplate))
	cmd := &cobra.Command{
		Use: "n26-csv",
		RunE: func(cmd *cobra.Command, args []string) error {
			if account == "" {
				return fmt.Errorf("please specify an account")
			}
			proc := NewN26CSVProcessor(os.Stdin)
			// Skip the header
			proc.reader.Read()
			bookings, err := proc.ReadAll(cmd.Context())
			if err != nil {
				return err
			}
			sort.Sort(importRecordByDate(bookings))
			for _, b := range bookings {
				b.Account = account
				b.Confirmed = confirmed
				if err := tmpl.Execute(os.Stdout, b); err != nil {
					return err
				}
			}
			return nil
		},
	}
	cmd.Flags().StringVar(&account, "account", "", "Account to target with this booking")
	cmd.Flags().BoolVar(&confirmed, "confirmed", false, "Are these bookings confirmed?")
	return cmd
}

func init() {
	rootCmd.AddCommand(generateImportCmd())
}
