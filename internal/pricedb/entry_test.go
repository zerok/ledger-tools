package pricedb_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/ledger-tools/internal/pricedb"
)

func TestEntryParsing(t *testing.T) {
	t.Run("common-valid", func(t *testing.T) {
		e, err := pricedb.ParseEntry("P 2021/02/06 10:36:00 XLM €0.2921")
		require.NoError(t, err)
		require.NotNil(t, e)
		require.Equal(t, "P", e.Type)
		require.Equal(t, "XLM", e.Currency)
		require.Equal(t, "€0.2921", e.RawValue)
		tm := time.Date(2021, 2, 6, 10, 36, 0, 0, time.UTC)
		require.Equal(t, tm, e.Time)
	})
}
