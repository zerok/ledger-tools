package pricedb

import (
	"bufio"
	"io"
	"strings"
)

type PriceDB struct {
	entries []Entry
}

func New() *PriceDB {
	return &PriceDB{
		entries: make([]Entry, 0, 10),
	}
}

func (db *PriceDB) Entries() []Entry {
	result := make([]Entry, 0, len(db.entries))
	result = append(result, db.entries...)
	return result
}

func Parse(in io.Reader) (*PriceDB, error) {
	db := New()
	reader := bufio.NewReader(in)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		l := strings.TrimSpace(line)
		if l == "" {
			continue
		}
		entry, err := ParseEntry(l)
		if err != nil {
			return nil, err
		}
		db.entries = append(db.entries, *entry)

	}
	return db, nil
}

func (db *PriceDB) AddRawEntry(raw string) error {
	e, err := ParseEntry(raw)
	if err != nil {
		return err
	}
	return db.AddEntry(*e)
}

func (db *PriceDB) AddEntry(e Entry) error {
	for _, ex := range db.entries {
		if e.Type == ex.Type && e.Currency == ex.Currency && e.Time == ex.Time && e.RawValue == ex.RawValue {
			return nil
		}

	}
	db.entries = append(db.entries, e)
	return nil
}
