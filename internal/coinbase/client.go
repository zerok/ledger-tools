package coinbase

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/shopspring/decimal"
)

type Client struct{}

func NewClient() *Client {
	return &Client{}
}

type Rates map[string]decimal.Decimal

type exchangeRatesResponse struct {
	Data struct {
		Currency string            `json:"currency"`
		Rates    map[string]string `json:"rates"`
	} `json:"data"`
}

func (cb *Client) GetExchangeRates(ctx context.Context, currency string) (Rates, error) {
	values := make(url.Values)
	values.Add("currency", currency)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://api.coinbase.com/v2/exchange-rates?"+values.Encode(), nil)
	if err != nil {
		return nil, err
	}
	hc := http.Client{}
	resp, err := hc.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code from Coinbase: %d", resp.StatusCode)
	}
	raw := exchangeRatesResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&raw); err != nil {
		return nil, err
	}
	result := make(Rates)
	for cur, rate := range raw.Data.Rates {
		result[cur], err = decimal.NewFromString(rate)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}
