package pricedb_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/ledger-tools/internal/pricedb"
)

func TestEncode(t *testing.T) {
	db := pricedb.New()
	out := bytes.Buffer{}

	require.NoError(t, pricedb.NewEncoder(&out).Encode(db))
	require.Equal(t, 0, out.Len())
	out.Reset()

	db.AddRawEntry("P 2021/04/01 12:04:00 ETH €1652.7")
	require.NoError(t, pricedb.NewEncoder(&out).Encode(db))
	require.Equal(t, "P 2021/04/01 12:04:00 ETH €1652.7\n", out.String())
	out.Reset()

	// Make sure that the output is date-sorted
	db.AddRawEntry("P 2021/04/01 12:06:00 ETH €1652.7")
	db.AddRawEntry("P 2021/04/01 12:04:00 ETH €1652.7")
	require.NoError(t, pricedb.NewEncoder(&out).Encode(db))
	require.Equal(t, "P 2021/04/01 12:04:00 ETH €1652.7\nP 2021/04/01 12:06:00 ETH €1652.7\n", out.String())
	out.Reset()
}
