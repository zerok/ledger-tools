package pricedb

import (
	"fmt"
	"io"
)

type Encoder struct {
	w io.Writer
}

type entriesByTime []Entry

func (e entriesByTime) Len() int {
	return len(e)
}

func (e entriesByTime) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e entriesByTime) Less(i, j int) bool {
	return e[i].Time.Before(e[j].Time)
}

func (e *Encoder) Encode(db *PriceDB) error {
	for _, entry := range db.Entries() {
		tm := entry.Time.Format("2006/01/02 15:04:05")
		line := fmt.Sprintf("%s %s %s %s\n", entry.Type, tm, entry.Currency, entry.RawValue)
		if _, err := e.w.Write([]byte(line)); err != nil {
			return err
		}
	}
	return nil
}

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{
		w: w,
	}
}
