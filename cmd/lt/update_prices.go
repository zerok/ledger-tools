package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/zerok/ledger-tools/internal/coinbase"
	"gitlab.com/zerok/ledger-tools/internal/pricedb"
)

func generateUpdatePricesCmd() *cobra.Command {
	currencyMapping := map[string]string{
		"EUR": "€",
		"USD": "$",
	}
	var priceDBFile string
	var baseCurrency string
	var overwrite bool
	var coinbaseCurrencies []string
	var cmd = &cobra.Command{
		Use: "update-prices",
		RunE: func(cmd *cobra.Command, args []string) error {
			baseCurrencySymbol, ok := currencyMapping[baseCurrency]
			if !ok {
				return fmt.Errorf("unsupported base currency")
			}
			fp, err := os.Open(priceDBFile)
			if err != nil {
				return err
			}
			defer fp.Close()
			db, err := pricedb.Parse(fp)
			if err != nil {
				return fmt.Errorf("failed to open %s: %w", priceDBFile, err)
			}
			cb := coinbase.NewClient()
			for _, coin := range coinbaseCurrencies {
				rates, err := cb.GetExchangeRates(context.Background(), coin)
				if err != nil {
					return err
				}
				e := pricedb.Entry{
					Type:     "P",
					Time:     time.Now(),
					Currency: coin,
					RawValue: baseCurrencySymbol + rates[baseCurrency].String(),
				}
				if err := db.AddEntry(e); err != nil {
					return err
				}
			}

			if !overwrite {
				return pricedb.NewEncoder(os.Stdout).Encode(db)
			}
			out, err := os.OpenFile(priceDBFile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
			if err != nil {
				return err
			}
			defer out.Close()
			return pricedb.NewEncoder(out).Encode(db)
		},
	}
	cmd.Flags().StringVar(&priceDBFile, "price-db", "prices.db", "Path to a prices-db file")
	cmd.Flags().StringVar(&baseCurrency, "base-currency", "EUR", "Base currency")
	cmd.Flags().BoolVar(&overwrite, "overwrite", false, "Overwrite the price-db file")
	cmd.Flags().StringSliceVar(&coinbaseCurrencies, "coinbase-currency", []string{"BTC", "ETH", "XLM"}, "Currencies to fetch from Coinbase")
	return cmd
}

func init() {
	rootCmd.AddCommand(generateUpdatePricesCmd())
}
