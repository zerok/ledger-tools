BIN := lt
all: bin/$(BIN)

bin:
	mkdir -p bin

clean:
	rm -rf dist bin

bin/$(BIN): $(shell find . -name '*.go') go.mod bin
	cd cmd/$(BIN) && go build -mod=mod -o ../../$@

test:
	go test -mod=mod ./... -v

.PHONY: clean all
