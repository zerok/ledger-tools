package pricedb_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/ledger-tools/internal/pricedb"
)

var testEntries = `
P 2021/04/01 12:04:00 XLM €0.3506425
P 2021/04/01 12:02:00 BTC €50195.63
P 2021/04/01 12:04:00 XLM €0.3506425
P 2021/04/01 12:04:00 ETH €1652.7
`

func TestPriceDBParsing(t *testing.T) {
	input := bytes.NewBufferString(testEntries)
	db, err := pricedb.Parse(input)
	require.NoError(t, err)
	require.NotNil(t, db)
	require.Len(t, db.Entries(), 4)
	require.Equal(t, "ETH", db.Entries()[3].Currency)
}

func TestAdd(t *testing.T) {
	db := pricedb.New()
	require.NoError(t, db.AddRawEntry("P 2021/04/01 12:04:00 XLM €0.3506425"))
	require.Len(t, db.Entries(), 1)

	// Let's now add a different entry and the count should increase:
	require.NoError(t, db.AddRawEntry("P 2021/04/01 12:04:00 ETH €1652.7"))
	require.Len(t, db.Entries(), 2)

	// If we now add that entry another time, the count should stay the same:
	require.NoError(t, db.AddRawEntry("P 2021/04/01 12:04:00 ETH €1652.7"))
	require.Len(t, db.Entries(), 2)
}
