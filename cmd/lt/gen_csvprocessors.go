package main

import "context"
import "encoding/csv"
import "io"
import "gitlab.com/zerok/csvgen/pkg/encoding"

func NewN26CSVProcessor(r io.Reader) *N26CSVProcessor {
	reader := csv.NewReader(encoding.WrapReaderForEncoding(r, "Windows1252"))
	reader.Comma = ','
	reader.FieldsPerRecord = 9
	result := &N26CSVProcessor{
		reader: reader,
	}
	return result
}

func (r *N26CSVProcessor) Read(ctx context.Context) (importRecord, error) {
	result := importRecord{}
	record, err := r.reader.Read()
	if err != nil {
		return result, err
	}
	return r.parseRecord(ctx, record)
}

func (r *N26CSVProcessor) ReadAll(ctx context.Context) ([]importRecord, error) {
	result := make([]importRecord, 0, 10)
	for {
		rec, err := r.Read(ctx)
		if err != nil {
			if err == io.EOF {
				break
			}
			return result, err
		}
		result = append(result, rec)
	}
	return result, nil
}

func (r *N26CSVProcessor) parseRecord(ctx context.Context, record []string) (importRecord, error) {
	var err error
	result := importRecord{}

	result.BookedAt, err = r.parseDate(
		ctx,
		record[0],
	)
	if err != nil {
		return result, err
	}

	result.Description = record[1]

	result.Value, err = r.parseValue(
		ctx,
		record[5],
	)
	if err != nil {
		return result, err
	}

	return result, nil
}

func NewEasybankCSVProcessor(r io.Reader) *EasybankCSVProcessor {
	reader := csv.NewReader(encoding.WrapReaderForEncoding(r, "Windows1252"))
	reader.Comma = ';'
	reader.FieldsPerRecord = 6
	result := &EasybankCSVProcessor{
		reader: reader,
	}
	return result
}

func (r *EasybankCSVProcessor) Read(ctx context.Context) (importRecord, error) {
	result := importRecord{}
	record, err := r.reader.Read()
	if err != nil {
		return result, err
	}
	return r.parseRecord(ctx, record)
}

func (r *EasybankCSVProcessor) ReadAll(ctx context.Context) ([]importRecord, error) {
	result := make([]importRecord, 0, 10)
	for {
		rec, err := r.Read(ctx)
		if err != nil {
			if err == io.EOF {
				break
			}
			return result, err
		}
		result = append(result, rec)
	}
	return result, nil
}

func (r *EasybankCSVProcessor) parseRecord(ctx context.Context, record []string) (importRecord, error) {
	var err error
	result := importRecord{}

	result.BookedAt, err = r.parseDate(
		ctx,
		record[3],
	)
	if err != nil {
		return result, err
	}

	result.Description = record[1]

	result.Value, err = r.parseValue(
		ctx,
		record[4],
	)
	if err != nil {
		return result, err
	}

	return result, nil
}
