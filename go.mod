module gitlab.com/zerok/ledger-tools

go 1.16

require (
	github.com/shopspring/decimal v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.7.0
	gitlab.com/zerok/csvgen v0.1.0
	golang.org/x/text v0.3.6 // indirect
)
