package pricedb

import (
	"fmt"
	"strings"
	"time"
)

type Entry struct {
	Type     string
	Time     time.Time
	Currency string
	RawValue string
}

func ParseEntry(e string) (*Entry, error) {
	elems := strings.Split(e, " ")
	if len(elems) != 5 {
		return nil, fmt.Errorf("invalid entry")
	}
	result := Entry{}
	result.Type = elems[0]
	result.RawValue = elems[4]
	result.Currency = elems[3]
	t, err := time.Parse("2006/01/02 15:04:05 Z", fmt.Sprintf("%s %s Z", elems[1], elems[2]))
	if err != nil {
		return nil, err
	}
	result.Time = t
	return &result, nil
}
